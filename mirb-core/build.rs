use serde_derive::Deserialize;
use std::collections::HashMap;
use std::env;
use std::fs::{self, File};
use std::io::{BufWriter, Write};
use std::path::Path;

fn generate_tags() {
    #[derive(Deserialize)]
    struct TempTag {
        #[serde(default)]
        negated_by: Vec<String>,
    }
    #[derive(Deserialize)]
    struct TagList {
        tags: HashMap<String, TempTag>,
    }

    let tags = fs::read_to_string("tags.toml").expect("missing tags file");
    let tags: TagList = toml::from_str(&tags).expect("failed to parse tags");
    fn to_src((name, tag): (&String, &TempTag)) -> String {
        format!(
            "(\"{0}\", Tag{{ name: \"{0}\", negated_by: LazyLock::new(|| [{1}].into()) }}),",
            name,
            tag.negated_by
                .iter()
                .map(|t| format!("TAGS.get({0:?}).expect(\"did not find tag '{0}'\")", t))
                .collect::<String>()
        )
    }
    let src = tags.tags.iter().map(to_src).collect::<String>();
    let path = Path::new(&env::var("OUT_DIR").unwrap()).join("tags_codegen.rs");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    write!(&mut file, "[{}]", src).unwrap();
}

fn generate_ingredients() {
    #[derive(Deserialize)]
    struct TempIng {
        #[serde(default)]
        tags: Vec<String>,
    }
    #[derive(Deserialize)]
    struct IngList {
        ingredients: HashMap<String, TempIng>,
    }

    let ings = fs::read_to_string("ingredients.toml").expect("missing ingredients file");
    let ings = toml::from_str::<IngList>(&ings).expect("failed to parse ingredients file");

    fn to_src((name, ing): (&String, &TempIng)) -> String {
        format!(
            "(\"{0}\", Ingredient {{ name: \"{0}\", tags: [{1}].into() }}),",
            name,
            ing.tags
                .iter()
                .map(|t| format!("TAGS.get(\"{}\").expect(\"tag '{0}' not found\"),", t))
                .collect::<String>()
        )
    }

    let src = ings.ingredients.iter().map(to_src).collect::<String>();
    let path = Path::new(&env::var("OUT_DIR").unwrap()).join("ings_codegen.rs");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    write!(&mut file, "[{}]", src).unwrap();
}

fn generate_cuisines() {
    #[derive(Deserialize)]
    struct TempCuisine {
        ingredients: Vec<String>,
        tags: Vec<String>,
        #[serde(default)]
        negative_tags: Vec<String>,
        tool: String,
    }
    #[derive(Deserialize)]
    struct CuiList {
        cuisines: HashMap<String, TempCuisine>,
    }
    let cuis = fs::read_to_string("cuisines.toml").expect("missing cuisines file");
    let cuis = toml::from_str::<CuiList>(&cuis).expect("failed to parse cuisines file");

    fn to_src((name, cui): (&String, &TempCuisine)) -> String {
        format!(
            "(\"{0}\", Cuisine {{ name: \"{0}\", ingredients: [{1}].into(), tags: [{2}].into(), negative_tags: [{3}].into(), tool: Tool::{4} }}),",
            name,
            cui.ingredients
                .iter()
                .map(|i| format!("INGREDIENTS.get(\"{0}\").expect(\"ingredient '{0}' not found\"),", i))
                .collect::<String>(),
            cui.tags
                .iter()
                .map(|t| format!("TAGS.get(\"{}\").expect(\"tag '{0}' not found\"),", t))
                .collect::<String>(),
            cui.negative_tags
                .iter()
                .map(|t| format!("TAGS.get(\"{}\").expect(\"tag '{0}' not found\"),", t))
                .collect::<String>(),
            cui.tool,
        )
    }

    let src = cuis.cuisines.iter().map(to_src).collect::<String>();
    let path = Path::new(&env::var("OUT_DIR").unwrap()).join("cuis_codegen.rs");
    let mut file = BufWriter::new(File::create(&path).unwrap());
    write!(&mut file, "[{}]", src).unwrap();
}

fn main() {
    println!("cargo:rerun-if-changed=tags.toml");
    println!("cargo:rerun-if-changed=ingredients.toml");
    println!("cargo:rerun-if-changed=cuisines.toml");
    generate_tags();
    generate_ingredients();
    generate_cuisines();
}
