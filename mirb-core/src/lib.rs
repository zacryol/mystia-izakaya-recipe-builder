#![feature(lazy_cell)]
use itertools::Itertools;
use std::{
    collections::{HashMap, HashSet},
    fmt::{self, Display},
    hash::Hash,
    sync::LazyLock,
};

#[derive(Debug)]
pub struct Tag {
    name: &'static str,
    negated_by: LazyLock<HashSet<&'static Tag>>,
}

impl Tag {
    #[allow(dead_code)]
    fn is_negatable(&self) -> bool {
        !self.negated_by.is_empty()
    }

    fn negates(&self, other: &Self) -> bool {
        other.negated_by.contains(self)
    }

    fn is_helpful_to(&self, pref: &Prefence) -> bool {
        pref.positive.iter().any(|tag| tag == &self)
            || pref.negative.iter().any(|tag| self.negates(tag))
    }

    fn is_relevant_to(&self, pref: &Prefence) -> bool {
        pref.positive
            .iter()
            .chain(pref.negative.iter())
            .any(|tag| tag == &self || self.negates(tag))
    }
}

impl PartialEq for Tag {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Eq for Tag {}

impl Hash for Tag {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state)
    }
}

#[derive(Debug, Eq)]
pub struct Ingredient {
    name: &'static str,
    tags: HashSet<&'static Tag>,
}

impl PartialEq for Ingredient {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl Hash for Ingredient {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.name.hash(state)
    }
}

#[derive(Debug)]
enum Tool {
    BoilingPot,
    Grill,
    FryingPan,
    Steamer,
    CuttingBoard,
}

#[derive(Debug)]
pub struct Cuisine {
    name: &'static str,
    ingredients: Vec<&'static Ingredient>,
    tags: HashSet<&'static Tag>,
    negative_tags: HashSet<&'static Tag>,
    tool: Tool,
}

static TAGS: LazyLock<HashMap<&'static str, Tag>> =
    LazyLock::new(|| include!(concat!(env!("OUT_DIR"), "/tags_codegen.rs")).into());

pub fn get_tag(name: &str) -> Option<&'static Tag> {
    TAGS.get(name)
}

static INGREDIENTS: LazyLock<HashMap<&'static str, Ingredient>> =
    LazyLock::new(|| include!(concat!(env!("OUT_DIR"), "/ings_codegen.rs")).into());

static CUISINES: LazyLock<HashMap<&'static str, Cuisine>> =
    LazyLock::new(|| include!(concat!(env!("OUT_DIR"), "/cuis_codegen.rs")).into());

pub struct Prefence {
    pub positive: HashSet<&'static Tag>,
    pub negative: HashSet<&'static Tag>,
}

#[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord, Clone, Copy)]
pub enum PrefenceMatches {
    NegativeMatch,
    Count(usize),
}

#[derive(Debug)]
pub struct Recipe {
    base: &'static Cuisine,
    extra: HashSet<&'static Ingredient>,
}

impl Recipe {
    const MAX_ING: usize = 5;

    fn new(base: &'static Cuisine, extra: HashSet<&'static Ingredient>) -> Option<Self> {
        Some(Self { base, extra }).filter(|r| r.total_ingredient_count() <= Self::MAX_ING)
    }

    fn get_all_tags(&self) -> HashSet<&'static Tag> {
        static LARGE_PORTION: LazyLock<&'static Tag> = LazyLock::new(|| &TAGS["Large Portion"]);
        //static SMALL_PORTION: LazyLock<&'static Tag> = LazyLock::new(|| &TAGS["Small Portion"]);

        let ing_tags: HashSet<_> = self
            .extra
            .iter()
            .flat_map(|ing| ing.tags.iter())
            .copied()
            .collect::<HashSet<_>>();
        let tags = self
            .base
            .tags
            .union(&ing_tags)
            .copied()
            .chain(
                [if self.total_ingredient_count() == Self::MAX_ING {
                    Some(*LARGE_PORTION)
                } else {
                    None
                }]
                .into_iter()
                .flatten(),
            )
            .collect::<HashSet<_>>();
        tags.iter()
            .filter(|tag| tags.intersection(&tag.negated_by).next().is_none())
            .copied()
            .collect()
    }

    fn get_match_count(&self, pref: &Prefence) -> PrefenceMatches {
        let tags = self.get_all_tags();
        if pref.negative.intersection(&tags).next().is_some() {
            PrefenceMatches::NegativeMatch
        } else {
            PrefenceMatches::Count(pref.positive.intersection(&tags).count())
        }
    }

    fn total_ingredient_count(&self) -> usize {
        self.base.ingredients.len() + self.extra.len()
    }

    fn validate(self) -> Option<Self> {
        if self
            .get_all_tags()
            .intersection(&self.base.negative_tags)
            .next()
            .is_some()
        {
            None
        } else {
            Some(self)
        }
    }

    fn is_redundant(&self, pref: &Prefence) -> bool {
        static LARGE_PORTION: LazyLock<&'static Tag> = LazyLock::new(|| &TAGS["Large Portion"]);
        if self.total_ingredient_count() >= Self::MAX_ING && pref.positive.contains(*LARGE_PORTION)
        {
            return false;
        }

        let relv_tags_per_ing = self
            .extra
            .iter()
            .map(|ing| {
                ing.tags
                    .iter()
                    .copied()
                    .filter(|t| t.is_relevant_to(pref))
                    .collect::<HashSet<_>>()
            })
            .collect_vec();
        !relv_tags_per_ing.iter().all(|tags| {
            tags.difference(&self.base.tags).next().is_some()
                && relv_tags_per_ing
                    .iter()
                    .filter(|&ts| ts as *const _ != tags as *const _)
                    .all(|ts| ts.difference(tags).next().is_some())
        })
    }
}

impl Display for Recipe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{} ({:?}) + {:?} on {:?}",
            self.base.name,
            self.base.ingredients.iter().map(|i| i.name).collect_vec(),
            self.extra.iter().map(|i| i.name).collect_vec(),
            self.base.tool
        )
    }
}

pub fn build_recipes(pref: &Prefence) -> Vec<(PrefenceMatches, Vec<Recipe>)> {
    fn recipes_from_cuisine(cui: &'static Cuisine, ings: &[&'static Ingredient]) -> Vec<Recipe> {
        (0..=(Recipe::MAX_ING - cui.ingredients.len()))
            .flat_map(|num| ings.iter().combinations(num))
            .filter_map(|ib| {
                Recipe::new(cui, ib.into_iter().copied().collect())
                    .expect("too many ingredients")
                    .validate()
            })
            .collect_vec()
    }
    let cuis = CUISINES
        .values()
        .filter(|c| c.tags.iter().any(|t| t.is_helpful_to(pref)))
        .collect::<Vec<_>>();
    let ings = INGREDIENTS
        .values()
        .filter(|ing| ing.tags.iter().any(|t| t.is_helpful_to(pref)))
        .collect::<Vec<_>>();

    cuis.into_iter()
        .flat_map(|c| recipes_from_cuisine(c, &ings))
        .filter(|recp| !recp.is_redundant(pref))
        .map(|recp| (recp.get_match_count(pref), recp))
        .into_group_map()
        .into_iter()
        .sorted_by_key(|(mc, _)| *mc)
        .collect_vec()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_valid_references() {
        let _ings = LazyLock::force(&INGREDIENTS);
        let _cuis = LazyLock::force(&CUISINES);
        for (_, tag) in TAGS.iter() {
            let _ = LazyLock::force(&tag.negated_by);
        }
    }

    #[test]
    fn test_get_all_tags() {
        let tags = Recipe {
            base: &CUISINES["Rice Ball"],
            extra: [&INGREDIENTS["Radish"]].into(),
        }
        .get_all_tags();
        assert!(tags.contains(&TAGS["Filling"]));
        assert!(!tags.contains(&TAGS["Good with Alcohol"]));
        assert!(tags.contains(&TAGS["Vegetarian"]));

        let tags = Recipe {
            base: &CUISINES["Rice Ball"],
            extra: [&INGREDIENTS["Radish"], &INGREDIENTS["Pork"]].into(),
        }
        .get_all_tags();
        assert!(tags.contains(&TAGS["Filling"]));
        assert!(!tags.contains(&TAGS["Good with Alcohol"]));
        assert!(tags.contains(&TAGS["Meat"]));
        assert!(!tags.contains(&TAGS["Vegetarian"]));

        let tags = Recipe::new(
            &CUISINES["Fresh Tofu"],
            INGREDIENTS.values().take(3).collect(),
        )
        .unwrap()
        .get_all_tags();
        assert!(tags.contains(&TAGS["Large Portion"]));
        assert!(!tags.contains(&TAGS["Small Portion"]));

        let tags = Recipe::new(&CUISINES["Roasted Mushroom"], [].into())
            .unwrap()
            .get_all_tags();
        assert!(!tags.contains(&TAGS["Savory"]));
    }

    #[test]
    fn test_redundancy() {
        let recp = Recipe::new(&CUISINES["Fresh Tofu"], [&INGREDIENTS["Seaweed"]].into()).unwrap();
        assert!(recp.is_redundant(&Prefence {
            positive: [&TAGS["Vegetarian"]].into(),
            negative: Default::default()
        }));
        assert!(!recp.is_redundant(&Prefence {
            positive: [&TAGS["Vegetarian"], &TAGS["Savory"]].into(),
            negative: Default::default()
        }));

        let recp = Recipe::new(
            &CUISINES["Lunar Dango"],
            [&INGREDIENTS["Lunar Herb"], &INGREDIENTS["Udumbara"]].into(),
        )
        .unwrap();
        assert!(recp.is_redundant(&Prefence {
            positive: [&TAGS["Japanese"], &TAGS["Dreamy"]].into(),
            negative: Default::default(),
        }));
    }
}
