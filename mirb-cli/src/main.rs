#![feature(result_option_inspect)]

use clap::{crate_version, Parser, ValueEnum};
use mirb_core::*;
use rand::seq::SliceRandom;
use std::{
    collections::HashSet,
    fmt::{self, Display},
};

#[derive(Parser, Debug)]
#[command(name = "Mystia's Izakaya Recipe Builder (CLI)")]
#[command(version = crate_version!())]
struct Args {
    #[arg(short, long, default_value_t = MatchMode::Best)]
    mode: MatchMode,
    #[arg(short)]
    /// tag to match for
    positive: Vec<String>,
    #[arg(short)]
    /// tag to avoid
    negative: Vec<String>,
}

#[derive(Debug, Clone, Copy, ValueEnum)]
enum MatchMode {
    /// only show the best possible matches, perfect or not
    Best,
    /// show the perfect matches
    Perfect,
    /// show all matches
    All,
    /// show one random match from the best
    One,
}

impl Display for MatchMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Best => "best",
                Self::Perfect => "perfect",
                Self::All => "all",
                Self::One => "one",
            }
        )
    }
}

fn main() -> Result<(), &'static str> {
    let args = Args::parse();
    if args.positive.is_empty() {
        eprintln!("Please specify at least one positive tag. use the `--help` flag for more info.");
        std::process::exit(1)
    }

    let pref = Prefence {
        positive: args
            .positive
            .iter()
            .map(|t| get_tag(t))
            .collect::<Option<HashSet<_>>>()
            .ok_or("invalid tag")?,
        negative: args
            .negative
            .iter()
            .map(|t| get_tag(t))
            .collect::<Option<HashSet<_>>>()
            .ok_or("invalid tag")?,
    };

    let recps = build_recipes(&pref);

    match args.mode {
        MatchMode::All => recps.iter().for_each(|(mc, recps)| {
            println!("{mc:?}");
            recps.iter().for_each(|recp| println!("{recp}\n"));
        }),
        MatchMode::Perfect => recps
            .last()
            .filter(|(mc, _)| matches!(mc, PrefenceMatches::Count(x) if *x == args.positive.len()))
            .ok_or("no perfect matches")?
            .1
            .iter()
            .for_each(|recp| println!("{recp}\n")),
        MatchMode::Best => recps
            .last()
            .inspect(|(mc, _)| println!("{mc:?}"))
            .ok_or("no matches")?
            .1
            .iter()
            .for_each(|recp| println!("{recp}\n")),
        MatchMode::One => println!(
            "{}",
            recps
                .last()
                .inspect(|(mc, _)| println!("{mc:?}"))
                .ok_or("no matches")?
                .1
                .choose(&mut rand::thread_rng())
                .ok_or("no matches")?
        ),
    }

    Ok(())
}
