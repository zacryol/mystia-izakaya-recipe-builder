# Mystia's Izakaya Recipe Builder

This tool is a little helper program I made for the game [Touhou Mystia's Izakaya](https://store.steampowered.com/app/1584090/Touhou_Mystias_Izakaya/).

Given a set of a character's preferences, it can attempt to create the most optimal recipe for those preferences.
